﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlfavoxApi.Models
{
    public interface IPlayerRepository
    {
        Player GetOnePlayer(int PlayerId);
        IEnumerable<Player> GetAllPlayers();
        IEnumerable<Player> GetPlayers(IEnumerable<int> ids);
        Player AddPlayer(Player player);
    }
}
