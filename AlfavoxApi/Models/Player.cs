﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlfavoxApi.Models
{
    public class Player
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Club { get; set; }
        public string Position { get; set; }
        public int Number { get; set; }
        public string Nationality { get; set; }
    }
}
