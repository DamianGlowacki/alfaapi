﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlfavoxApi.Models
{
    public class PlayerRepository : IPlayerRepository
    {

        private readonly AppDbContext _appDbContext;

        public PlayerRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }


        public IEnumerable<Player> GetAllPlayers()
        {
            return _appDbContext.Players;
        }

        public Player GetOnePlayer(int PlayerId)
        {
            return _appDbContext.Players.FirstOrDefault(x => x.Id == PlayerId);
        }

        public IEnumerable<Player> GetPlayers(IEnumerable<int> ids)
        {
            return _appDbContext.Players.Where(x => ids.Contains(x.Id));
        }

        public Player AddPlayer(Player player)
        {
            _appDbContext.Add(player);
            _appDbContext.SaveChanges();
            return player;
        }
    }
}
