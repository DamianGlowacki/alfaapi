﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlfavoxApi.Models
{
    public static class DbInitializer
    {
        public static void Seed(AppDbContext context)
        {
            if (!context.Players.Any())
            {
                context.AddRange(
                    new Player { FirstName = "Robert", LastName = "Lewandowski", Number = 9, Club = "Bayern Munchen", Position = "Central Forwarder", Nationality = "Poland" },
                    new Player { FirstName = "Eden", LastName = "Hazard", Number = 10, Club = "Chelsea", Position = "Left Winger", Nationality = "Belgium" },
                    new Player { FirstName = "Antoine", LastName = "Griezmann", Number = 7, Club = "Atletico Madrid", Position = "Central Forwarder", Nationality = "France" },
                    new Player { FirstName = "Luka", LastName = "Modrić", Number = 10, Club = "Real Madrid", Position = "Central Midfielder", Nationality = "Croatia" },
                    new Player { FirstName = "Giorgio", LastName = "Chiellini", Number = 3, Club = "Juventus", Position = "Central Defender", Nationality = "Italy" },
                    new Player { FirstName = "David", LastName = "De Gea", Number = 1, Club = "Manchester United", Position = "Goalkeeper", Nationality = "Spain" }
                    );
            }
            context.SaveChanges();
        }
    }
}
