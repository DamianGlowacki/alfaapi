﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AlfavoxApi.Models;

namespace AlfavoxApi.Controllers
{
    [Route("api/players/")]
    public class PlayersApiController : Controller
    {
        private readonly IPlayerRepository _players;

        public PlayersApiController(IPlayerRepository players)
        {
            _players = players;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlayers(int? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            var player = _players.GetOnePlayer(id.Value);
            if (player == null)
            {
                return NotFound();
            }

            return Ok(player);
        }

        [HttpGet]
        public async Task<IActionResult> GetPlayers([FromQuery] string ids)
        {
            if (!string.IsNullOrEmpty(ids))
            {
                var somePlayers = _players.GetPlayers(ids.Split(",").Select(x => int.Parse(x)).ToList());
                if (somePlayers == null)
                {
                    return NotFound();
                }

                return Ok(somePlayers);
            }

            var players = _players.GetAllPlayers();
            if (players == null)
            {
                return NotFound();
            }

            return Ok(players);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Player player)
        {
            if (ModelState.IsValid)
            {
                player = _players.AddPlayer(player);
                return Ok(player);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
