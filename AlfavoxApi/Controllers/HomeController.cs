﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AlfavoxApi.Models;

namespace AlfavoxApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPlayerRepository _playerRepository;

        public HomeController(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public IActionResult Index()
        {
            ViewBag.Tytul = "Lista zawodników";

            var players = _playerRepository.GetAllPlayers().OrderBy(x => x.LastName);

            return View(players);
        }
    }
}
